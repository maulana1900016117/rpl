<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php')) {
    require SYSTEMPATH . 'Config/Routes.php';
}

/*
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(true);

/*
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->get('/', 'Pages::index');

// MENU
$routes->get('/menu/create', 'Menu::create');
$routes->get('/menu/edit/(:any)', 'Menu::edit/$1');
$routes->delete('/menu/(:num)', 'Menu::delete/$1');
$routes->get('/menu/(:any)', 'Menu::details/$1');

// Barista
$routes->get('/barista/create', 'Barista::create');
$routes->get('/barista/edit/(:any)', 'Barista::edit/$1');
$routes->delete('/barista/(:num)', 'Barista::delete/$1');
$routes->get('/barista/(:any)', 'Barista::details/$1');

// pelanggan
$routes->get('/pelanggan/create', 'Pelanggan::create');
$routes->get('/pelanggan/edit/(:any)', 'Pelanggan::edit/$1');
$routes->delete('/pelanggan/(:num)', 'Pelanggan::delete/$1');
$routes->get('/pelanggan/(:any)', 'Pelanggan::details/$1');

// Pesanan
$routes->get('/pesanan/create', 'Pesanan::create');
$routes->get('/pesanan/edit/(:any)', 'Pesanan::edit/$1');
$routes->delete('/pesanan/(:num)', 'Pesanan::delete/$1');
$routes->get('/pesanan/(:any)', 'Pesanan::details/$1');


/*
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php')) {
    require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
