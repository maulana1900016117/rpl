<?php

namespace App\Models;

use CodeIgniter\Model;

class BaristaModel extends Model
{
    protected $table = 'barista';
    protected $primaryKey = 'id_barista';
    protected $allowedFields = ['NamaBarista', 'Alamat', 'NomorTlp'];

    public function getBarista($id_barista = false)
    {
        if ($id_barista == false) {
            return $this->findAll();
        }

        return $this->where(['id_barista' => $id_barista])->first();
    }
}
