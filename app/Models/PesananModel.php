<?php

namespace App\Models;

use CodeIgniter\Model;

class PesananModel extends Model
{
    protected $table = 'pesanan';
    protected $primaryKey = 'id_pesanan';
    protected $allowedFields = ['id_pesanan', 'id_barista', 'id_pelanggan'];

    public function getPesanan($id_pesanan = false)
    {
        if ($id_pesanan == false) {
            return $this->findAll();
        }

        return $this->where(['id_pesanan' => $id_pesanan])->first();
    }
}
