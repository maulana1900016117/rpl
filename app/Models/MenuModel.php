<?php

namespace App\Models;

use CodeIgniter\Model;

class MenuModel extends Model
{
    protected $table = 'menu';
    protected $primaryKey = 'id_menu';
    protected $allowedFields = ['Nama', 'Harga', 'Stok'];

    public function getMenu($id_menu = false)
    {
        if ($id_menu == false) {
            return $this->findAll();
        }

        return $this->where(['id_menu' => $id_menu])->first();
    }
}
