<?php

namespace App\Models;

use CodeIgniter\Model;

class PelangganModel extends Model
{
    protected $table = 'pelanggan';
    protected $primaryKey = 'id_pelanggan';
    protected $allowedFields = ['NamaPelanggan', 'Email', 'No_Tlp'];

    public function getPelanggan($id_pelanggan = false)
    {
        if ($id_pelanggan == false) {
            return $this->findAll();
        }

        return $this->where(['id_pelanggan' => $id_pelanggan])->first();
    }
}
