<?php

namespace App\Controllers;

use App\Models\PesananModel;

class Pesanan extends BaseController
{
    protected $pesananModel;
    public function __construct()
    {
        $this->pesananModel = new PesananModel();
    }

    public function index()
    {
        $data = [
            'title' => 'Daftar Pesanan',
            'pesanan' => $this->pesananModel->getPesanan()
        ];
        return view('pesanan/index', $data);
    }

    public function details($id_pesanan)
    {
        $data = [
            'title' => 'Detail Pesanan',
            'pesanan' => $this->pesananModel->getPesanan($id_pesanan)
        ];

        if (empty($data['pesanan'])) {
            throw new \CodeIgniter\Exceptions\PageNotFoundException('Daftar Pesanan ' . $id_pesanan . ' Tidak Ditemukan');
        }
        return view('pesanan/details', $data);
    }

    public function create()
    {

        $data = [
            'title' => 'Form Menambah Data Pesanan',
            'validation' => \Config\Services::validation()
        ];
        return view('pesanan/create', $data);
    }

    public function save()
    {
        // session();
        if (!$this->validate([
            'id_pesanan' => 'required',
            'id_barista' => 'required',
            'id_pelanggan' => 'required'
        ])) {
            $validation = \Config\Services::validation();
            return redirect()->to('/pesanan/create')->withInput()->with('validation', $validation);
        };
        $this->pesananModel->save([
            'id_pesanan' => $this->request->getVar('id_pesanan'),
            'id_barista' => $this->request->getVar('id_barista'),
            'id_pelanggan' => $this->request->getVar('id_pelanggan'),
        ]);

        session()->setFlashdata('alert', 'Data Berhasil Ditambahkan');

        return redirect()->to('/pesanan');
    }

    public function delete($id_pesanan)
    {
        $this->pesananModel->delete($id_pesanan);
        session()->setFlashdata('alert', 'Data Berhasil Dihapus');
        return redirect()->to('/pesanan');
    }

    public function edit($id_pesanan)
    {
        $data = [
            'title' => 'Form Merubah Data Pesanan',
            'validation' => \Config\Services::validation(),
            'pesanan' => $this->pesananModel->getPesanan($id_pesanan)
        ];
        return view('pesanan/edit', $data);
    }

    public function update($id_pesanan)
    {
        if (!$this->validate([
            'id_pesanan' => 'required',
            'id_barista' => 'required',
            'id_pelanggan' => 'required'
        ])) {
            $validation = \Config\Services::validation();
            return redirect()->to('/pesanan/create')->withInput()->with('validation', $validation);
        };
        $this->pesananModel->save([
            'id_pesanan' => $id_pesanan,
            'id_pesanan' => $this->request->getVar('id_pesanan'),
            'id_barista' => $this->request->getVar('id_barista'),
            'id_pelanggan' => $this->request->getVar('id_pelanggan'),
        ]);

        session()->setFlashdata('alert', 'Data Berhasil Ditambahkan');

        return redirect()->to('/pesanan');
    }
}
