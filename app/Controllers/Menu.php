<?php

namespace App\Controllers;

use App\Models\MenuModel;

class Menu extends BaseController
{
    protected $menuModel;
    public function __construct()
    {
        $this->menuModel = new MenuModel();
    }

    public function index()
    {
        $data = [
            'title' => 'Daftar Menu',
            'menu' => $this->menuModel->getMenu()
        ];
        return view('menu/index', $data);
    }

    public function details($id_menu)
    {
        $data = [
            'title' => 'Detail Menu',
            'menu' => $this->menuModel->getMenu($id_menu)
        ];

        if (empty($data['menu'])) {
            throw new \CodeIgniter\Exceptions\PageNotFoundException('Daftar Menu ' . $id_menu . ' Tidak Ditemukan');
        }
        return view('menu/details', $data);
    }

    public function create()
    {

        $data = [
            'title' => 'Form Menambah Data Menu',
            'validation' => \Config\Services::validation()
        ];
        return view('menu/create', $data);
    }

    public function save()
    {
        // session();
        if (!$this->validate([
            'Nama' => 'required',
            'Harga' => 'required',
            'Stok' => 'required'
        ])) {
            $validation = \Config\Services::validation();
            return redirect()->to('/menu/create')->withInput()->with('validation', $validation);
        };
        $this->menuModel->save([
            'Nama' => $this->request->getVar('Nama'),
            'Harga' => $this->request->getVar('Harga'),
            'Stok' => $this->request->getVar('Stok'),
        ]);

        session()->setFlashdata('alert', 'Data Berhasil Ditambahkan');

        return redirect()->to('/menu');
    }

    public function delete($id_menu)
    {
        $this->menuModel->delete($id_menu);
        session()->setFlashdata('alert', 'Data Berhasil Dihapus');
        return redirect()->to('/menu');
    }

    public function edit($id_menu)
    {
        $data = [
            'title' => 'Form Merubah Data Menu',
            'validation' => \Config\Services::validation(),
            'menu' => $this->menuModel->getMenu($id_menu)
        ];
        return view('menu/edit', $data);
    }

    public function update($id_menu)
    {
        if (!$this->validate([
            'Nama' => 'required',
            'Harga' => 'required',
            'Stok' => 'required'
        ])) {
            $validation = \Config\Services::validation();
            return redirect()->to('/menu/create')->withInput()->with('validation', $validation);
        };
        $this->menuModel->save([
            'id_menu' => $id_menu,
            'Nama' => $this->request->getVar('Nama'),
            'Harga' => $this->request->getVar('Harga'),
            'Stok' => $this->request->getVar('Stok'),
        ]);

        session()->setFlashdata('alert', 'Data Berhasil Ditambahkan');

        return redirect()->to('/menu');
    }
}
