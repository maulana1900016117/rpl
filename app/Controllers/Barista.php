<?php

namespace App\Controllers;

use App\Models\BaristaModel;

class Barista extends BaseController
{
    protected $baristaModel;
    public function __construct()
    {
        $this->baristaModel = new BaristaModel();
    }

    public function index()
    {
        $data = [
            'title' => 'Daftar Barista',
            'barista' => $this->baristaModel->getBarista()
        ];
        return view('barista/index', $data);
    }

    public function details($id_barista)
    {
        $data = [
            'title' => 'Detail Barista',
            'barista' => $this->baristaModel->getBarista($id_barista)
        ];

        if (empty($data['barista'])) {
            throw new \CodeIgniter\Exceptions\PageNotFoundException('Daftar barista ' . $id_barista . ' Tidak Ditemukan');
        }
        return view('barista/details', $data);
    }

    public function create()
    {

        $data = [
            'title' => 'Form Menambah Data barista',
            'validation' => \Config\Services::validation()
        ];
        return view('barista/create', $data);
    }

    public function save()
    {
        // session();
        if (!$this->validate([
            'NamaBarista' => 'required',
            'Alamat' => 'required',
            'NomorTlp' => 'required'
        ])) {
            $validation = \Config\Services::validation();
            return redirect()->to('/barista/create')->withInput()->with('validation', $validation);
        };
        $this->baristaModel->save([
            'NamaBarista' => $this->request->getVar('NamaBarista'),
            'Alamat' => $this->request->getVar('Alamat'),
            'NomorTlp' => $this->request->getVar('NomorTlp'),
        ]);

        session()->setFlashdata('alert', 'Data Berhasil Ditambahkan');

        return redirect()->to('/barista');
    }

    public function delete($id_barista)
    {
        $this->baristaModel->delete($id_barista);
        session()->setFlashdata('alert', 'Data Berhasil Dihapus');
        return redirect()->to('/barista');
    }

    public function edit($id_barista)
    {
        $data = [
            'title' => 'Form Merubah Data barista',
            'validation' => \Config\Services::validation(),
            'barista' => $this->baristaModel->getBarista($id_barista)
        ];
        return view('barista/edit', $data);
    }

    public function update($id_barista)
    {
        if (!$this->validate([
            'NamaBarista' => 'required',
            'Alamat' => 'required',
            'NomorTlp' => 'required'
        ])) {
            $validation = \Config\Services::validation();
            return redirect()->to('/barista/create')->withInput()->with('validation', $validation);
        };
        $this->baristaModel->save([
            'id_barista' => $id_barista,
            'NamaBarista' => $this->request->getVar('NamaBarista'),
            'Alamat' => $this->request->getVar('Alamat'),
            'NomorTlp' => $this->request->getVar('NomorTlp'),
        ]);

        session()->setFlashdata('alert', 'Data Berhasil Ditambahkan');

        return redirect()->to('/barista');
    }
}
