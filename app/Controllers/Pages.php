<?php

namespace App\Controllers;

class Pages extends BaseController
{
    public function index()
    {
        $data = [
            'title' => 'Home | Home',
            'angka' => ['1', '2', '3']
        ];
        echo view('pages/home', $data);
    }

    public function about()
    {
        $data = [
            'title' => 'About Us'
        ];

        echo view('pages/about', $data);
    }
}
