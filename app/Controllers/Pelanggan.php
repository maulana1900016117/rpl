<?php

namespace App\Controllers;

use App\Models\PelangganModel;

class Pelanggan extends BaseController
{
    protected $pelangganModel;
    public function __construct()
    {
        $this->pelangganModel = new PelangganModel();
    }

    public function index()
    {
        $data = [
            'title' => 'Daftar Pelanggan',
            'pelanggan' => $this->pelangganModel->getPelanggan()
        ];
        return view('pelanggan/index', $data);
    }

    public function details($id_pelanggan)
    {
        $data = [
            'title' => 'Detail Pelanggan',
            'pelanggan' => $this->pelangganModel->getPelanggan($id_pelanggan)
        ];

        if (empty($data['pelanggan'])) {
            throw new \CodeIgniter\Exceptions\PageNotFoundException('Daftar Pelanggan ' . $id_pelanggan . ' Tidak Ditemukan');
        }
        return view('pelanggan/details', $data);
    }

    public function create()
    {

        $data = [
            'title' => 'Form Menambah Data Pelanggan',
            'validation' => \Config\Services::validation()
        ];
        return view('pelanggan/create', $data);
    }

    public function save()
    {
        // session();
        if (!$this->validate([
            'NamaPelanggan' => 'required',
            'Email' => 'required',
            'No_Tlp' => 'required'
        ])) {
            $validation = \Config\Services::validation();
            return redirect()->to('/pelanggan/create')->withInput()->with('validation', $validation);
        };
        $this->pelangganModel->save([
            'NamaPelanggan' => $this->request->getVar('NamaPelanggan'),
            'Email' => $this->request->getVar('Email'),
            'No_Tlp' => $this->request->getVar('No_Tlp'),
        ]);

        session()->setFlashdata('alert', 'Data Berhasil Ditambahkan');

        return redirect()->to('/pelanggan');
    }

    public function delete($id_pelanggan)
    {
        $this->pelangganModel->delete($id_pelanggan);
        session()->setFlashdata('alert', 'Data Berhasil Dihapus');
        return redirect()->to('/pelanggan');
    }

    public function edit($id_pelanggan)
    {
        $data = [
            'title' => 'Form Merubah Data Pelanggan',
            'validation' => \Config\Services::validation(),
            'pelanggan' => $this->pelangganModel->getPelanggan($id_pelanggan)
        ];
        return view('pelanggan/edit', $data);
    }

    public function update($id_pelanggan)
    {
        if (!$this->validate([
            'NamaPelanggan' => 'required',
            'Email' => 'required',
            'No_Tlp' => 'required'
        ])) {
            $validation = \Config\Services::validation();
            return redirect()->to('/pelanggan/create')->withInput()->with('validation', $validation);
        };
        $this->pelangganModel->save([
            'id_pelanggan' => $id_pelanggan,
            'NamaPelanggan' => $this->request->getVar('NamaPelanggan'),
            'Email' => $this->request->getVar('Email'),
            'No_Tlp' => $this->request->getVar('No_Tlp'),
        ]);

        session()->setFlashdata('alert', 'Data Berhasil Ditambahkan');

        return redirect()->to('/pelanggan');
    }
}
