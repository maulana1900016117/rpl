<?= $this->extend("layout/tamplate"); ?>

<?= $this->section("content") ?>
<div class="container" style="margin-top:10px;">
    <div class="row">
        <div class="col">
            <h2>Form Menambah Data pelanggan</h2>
            <form action="/pelanggan/save" method="post">
                <?= csrf_field(); ?>
                <div class="row mb-3">
                    <label for="NamaPelanggan" class="col-sm-2 col-form-label">Nama Pelanggan</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control <?= $validation->hasError('NamaPelanggan') ? 'is-invalid' : '' ?>" id="NamaPelanggan" name="NamaPelanggan" value="<?= old('NamaPelanggan') ?>">
                    </div>
                    <div class="invalid-feedback">Example invalid select feedback</div>
                </div>
                <div class="row mb-3">
                    <label for="Email" class="col-sm-2 col-form-label">Email</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control <?= $validation->hasError('Email') ? 'is-invalid' : '' ?>" id="Email" name="Email" value="<?= old('Email') ?>">
                    </div>
                </div>
                <div class="row mb-3">
                    <label for="No_Tlp" class="col-sm-2 col-form-label">Nomor Telepon</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control <?= $validation->hasError('No_Tlp') ? 'is-invalid' : '' ?>" id="No_Tlp" name="No_Tlp" value="<?= old('No_Tlp') ?>">
                    </div>
                </div>

                <div class="d-grid gap-2 d-md-flex justify-content-md-end">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>
<?= $this->endSection() ?>