<?= $this->extend('layout/tamplate'); ?>

<?= $this->section('content'); ?>
<div class="container">
    <div class="row">
        <div class="col">
            <h1 style="margin: 5px;">Daftar pelanggan</h1>
            <a href="/pelanggan/create" class="btn btn-primary mb-2">Tambah Data pelanggan</a>
            <?php if (session()->setFlashdata('alert')) : ?>
                <div class="alert alert-success">
                    <?= session()->setFlashdata('alert') ?>
                </div>
            <?php endif ?>
            <table class="table table-bordered border-dark text-center">
                <thead>
                    <tr>
                        <th scope="row">
                            <h5>No.</h5>
                        </th>
                        <td>
                            <h5>Nama pelanggan<h5>
                        </td>
                        <td>
                            <h5>Email</h5>
                        </td>
                        <td>
                            <h5>No_Tlp</h5>
                        </td>
                        <td>
                            <h5>Action</h5>
                        </td>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($pelanggan as $pl) : ?>
                        <tr>
                            <th scope="row"><?= $pl['id_pelanggan'] ?></th>
                            <td><?= $pl['NamaPelanggan'] ?> </td>
                            <td><?= $pl['Email'] ?></td>
                            <td><?= $pl['No_Tlp'] ?></td>
                            <td><a href="/pelanggan/<?= $pl['id_pelanggan'] ?>" class="btn btn-success">Details</a></td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<?= $this->endSection('content'); ?>