<?= $this->extend("layout/tamplate"); ?>


<?= $this->section("content") ?>
<div class="container" style="margin-top: 10px;">
    <div class="row">
        <div class="col">
            <h2 class="mt-2">Detail pelanggan</h2>
            <div class="card mb-3" style="max-width: 540px;">
                <div class="row g-0">
                    <div class="col-md-4">
                        <img src="" class="card-img" alt="...">
                    </div>
                    <div class="col-md-8">
                        <div class="card-body">
                            <h5 class="card-title"><?= $pelanggan['NamaPelanggan'] ?></h5>
                            <p class="card-text">Email: <?= $pelanggan['Email'] ?></p>

                            <a href="/pelanggan/edit/<?= $pelanggan['id_pelanggan'] ?>" class="btn btn-warning">Edit</a>

                            <form action="/pelanggan/<?= $pelanggan['id_pelanggan'] ?>" method="post" class="d-inline">
                                <?= csrf_field() ?>
                                <input type="hidden" name="_method" value="DELETE">
                                <button class="btn btn-danger" onclick="return confirm('Yakin Ingin Menghapus Data?')">Delete</button>
                            </form>
                            <div class="d-grid gap-2" style="margin-top: 5px;">
                                <a href="/pelanggan" class="btn btn-primary">Kembali Ke Daftar pelanggan</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>