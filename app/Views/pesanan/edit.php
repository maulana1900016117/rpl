<?= $this->extend("layout/tamplate"); ?>

<?= $this->section("content") ?>
<div class="container" style="margin-top:10px;">
    <div class="row">
        <div class="col">
            <h2>Form Merubah Data pelanggan</h2>
            <form action="/pesanan/update/<?= $pesanan['id_pesanan'] ?>" method="post">
                <?= csrf_field(); ?>
                <div class="row mb-3">
                    <label for="id_pesanan" class="col-sm-2 col-form-label">ID_Pesanan</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control <?= $validation->hasError('id_pesanan') ? 'is-invalid' : '' ?>" id="id_pesanan" name="id_pesanan" value="<?= $pesanan['id_pesanan'] ?>">
                    </div>
                    <div class="invalid-feedback">Example invalid select feedback</div>
                </div>
                <div class="row mb-3">
                    <label for="id_barista" class="col-sm-2 col-form-label">ID_Barisata</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control <?= $validation->hasError('id_barista') ? 'is-invalid' : '' ?>" id="id_barista" name="id_barista" value="<?= $pesanan['id_barista'] ?>">
                    </div>
                </div>
                <div class="row mb-3">
                    <label for="id_pelanggan" class="col-sm-2 col-form-label">ID_Pelanggan</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control <?= $validation->hasError('id_pelanggan') ? 'is-invalid' : '' ?>" id="id_pelanggan" name="id_pelanggan" value="<?= $pesanan['id_pelanggan'] ?>">
                    </div>
                </div>

                <div class="d-grid gap-2 d-md-flex justify-content-md-end">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>
<?= $this->endSection() ?>