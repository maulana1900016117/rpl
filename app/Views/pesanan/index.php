<?= $this->extend('layout/tamplate'); ?>

<?= $this->section('content'); ?>
<div class="container">
    <div class="row">
        <div class="col">
            <h1 style="margin: 10px;">Daftar Pesanan</h1>
            <a href="/pesanan/create" class="btn btn-primary mb-2">Tambah Data pesanan</a>
            <?php if (session()->setFlashdata('alert')) : ?>
                <div class="alert alert-success">
                    <?= session()->setFlashdata('alert') ?>
                </div>
            <?php endif ?>
            <table class="table table-bordered border-dark text-center">
                <thead>
                    <tr>
                        <th scope="row">
                            <h5>No.</h5>
                        </th>
                        <td>
                            <h5>No. Pesanan<h5>
                        </td>
                        <td>
                            <h5>id_barista</h5>
                        </td>
                        <td>
                            <h5>id_pesanan</h5>
                        </td>
                        <td>
                            <h5>Action</h5>
                        </td>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($pesanan as $ps) : ?>
                        <tr>
                            <th scope="row"><?= $ps['id_pesanan'] ?></th>
                            <td><?= $ps['id_pesanan'] ?> </td>
                            <td><?= $ps['id_barista'] ?></td>
                            <td><?= $ps['id_pelanggan'] ?></td>
                            <td><a href="/pesanan/<?= $ps['id_pesanan'] ?>" class="btn btn-success">Details</a></td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<?= $this->endSection('content'); ?>