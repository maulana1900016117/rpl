<?= $this->extend('layout/tamplate'); ?>

<?= $this->section('content'); ?>
<div class="container">
    <div class="row">
        <div class="col">
            <h1 style="margin: 5px;">Daftar Barista</h1>
            <a href="/barista/create" class="btn btn-primary mb-2">Tambah Data Barista</a>
            <?php if (session()->setFlashdata('alert')) : ?>
                <div class="alert alert-success">
                    <?= session()->setFlashdata('alert') ?>
                </div>
            <?php endif ?>
            <table class="table table-bordered border-dark text-center">
                <thead>
                    <tr>
                        <th scope="row">
                            <h5>No.</h5>
                        </th>
                        <td>
                            <h5>Nama Barista<h5>
                        </td>
                        <td>
                            <h5>Alamat</h5>
                        </td>
                        <td>
                            <h5>No. Tlp</h5>
                        </td>
                        <td>
                            <h5>Action</h5>
                        </td>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($barista as $b) : ?>
                        <tr>
                            <th scope="row"><?= $b['id_barista'] ?></th>
                            <td><?= $b['NamaBarista'] ?> </td>
                            <td><?= $b['Alamat'] ?></td>
                            <td><?= $b['NomorTlp'] ?></td>
                            <td><a href="/barista/<?= $b['id_barista'] ?>" class="btn btn-success">Details</a></td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<?= $this->endSection('content'); ?>