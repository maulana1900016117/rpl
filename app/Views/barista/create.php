<?= $this->extend("layout/tamplate"); ?>

<?= $this->section("content") ?>
<div class="container" style="margin-top:10px;">
    <div class="row">
        <div class="col">
            <h2>Form Menambah Data Barista</h2>
            <form action="/barista/save" method="post">
                <?= csrf_field(); ?>
                <div class="row mb-3">
                    <label for="NamaBarista" class="col-sm-2 col-form-label">Nama Barista</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control <?= $validation->hasError('NamaBarista') ? 'is-invalid' : '' ?>" id="NamaBarista" name="NamaBarista" value="<?= old('NamaBarista') ?>">
                    </div>
                    <div class="invalid-feedback">Example invalid select feedback</div>
                </div>
                <div class="row mb-3">
                    <label for="Alamat" class="col-sm-2 col-form-label">Alamat</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control <?= $validation->hasError('Alamat') ? 'is-invalid' : '' ?>" id="Alamat" name="Alamat" value="<?= old('Alamat') ?>">
                    </div>
                </div>
                <div class="row mb-3">
                    <label for="NomorTlp" class="col-sm-2 col-form-label">NomorTlp</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control <?= $validation->hasError('NomorTlp') ? 'is-invalid' : '' ?>" id="NomorTlp" name="NomorTlp" value="<?= old('NomorTlp') ?>">
                    </div>
                </div>

                <div class="d-grid gap-2 d-md-flex justify-content-md-end">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>
<?= $this->endSection() ?>