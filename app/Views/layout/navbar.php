<div class="card">
    <div class="card-header">
        <ul class="nav nav-pills card-header-pills">
            <li class="nav-item">
                <a class="nav-link active" href="/">Home</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/menu">Menu</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/barista">Barista</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/pelanggan">Pelanggan</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/pesanan">Pesanan</a>
            </li>
        </ul>
    </div>
</div>