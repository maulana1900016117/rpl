<?= $this->extend('layout/tamplate'); ?>


<?= $this->section('content'); ?>
<div class="container">
    <div class="row">
        <div class="col text-center" style="margin-bottom: 5px;">
            <h1 style="margin-top: 15px;">Selamat Datang!</h1>
            <p>Web/App Kopi Gunung Express</p>

            <div class="d-grid gap-2 col-6 mx-auto">
                <a href="/menu" class="btn btn-dark text-white btn-lg" role="button">Menu</a>
                <a href="/barista" class="btn btn-dark text-white btn-lg" type="button">Barista</a>
                <a href="/pelanggan" class="btn btn-dark text-white btn-lg" type="button">Pelanggan</a>
                <a href="/pesanan" class="btn btn-dark text-white btn-lg" type="button">Pesanan</a>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection('content'); ?>