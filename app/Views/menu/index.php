<?= $this->extend('layout/tamplate'); ?>

<?= $this->section('content'); ?>
<div class="container">
    <div class="row">
        <div class="col">
            <h1 style="margin: 5px;">Daftar Menu</h1>
            <a href="/menu/create" class="btn btn-primary mb-2">Tambah Data Menu</a>
            <?php if (session()->setFlashdata('alert')) : ?>
                <div class="alert alert-success">
                    <?= session()->setFlashdata('alert') ?>
                </div>
            <?php endif ?>
            <table class="table table-bordered border-dark text-center">
                <thead>
                    <tr>
                        <th scope="row">
                            <h5>No.</h5>
                        </th>
                        <td>
                            <h5>Nama Menu<h5>
                        </td>
                        <td>
                            <h5>Harga</h5>
                        </td>
                        <td>
                            <h5>Stok</h5>
                        </td>
                        <td>
                            <h5>Action</h5>
                        </td>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($menu as $m) : ?>
                        <tr>
                            <th scope="row"><?= $m['id_menu'] ?></th>
                            <td><?= $m['Nama'] ?> </td>
                            <td><?= $m['Harga'] ?></td>
                            <td><?= $m['Stok'] ?></td>
                            <td><a href="/menu/<?= $m['id_menu'] ?>" class="btn btn-success">Details</a></td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<?= $this->endSection('content'); ?>