<?= $this->extend("layout/tamplate"); ?>

<?= $this->section("content") ?>
<div class="container" style="margin-top:10px;">
    <div class="row">
        <div class="col">
            <h2>Form Merubah Data Menu</h2>
            <form action="/menu/update/<?= $menu['id_menu'] ?>" method="post">
                <?= csrf_field(); ?>
                <div class="row mb-3">
                    <label for="Nama" class="col-sm-2 col-form-label">Nama Menu</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control <?= $validation->hasError('Nama') ? 'is-invalid' : '' ?>" id="Nama" name="Nama" value="<?= $menu['Nama'] ?>">
                    </div>
                    <div class="invalid-feedback">Example invalid select feedback</div>
                </div>
                <div class="row mb-3">
                    <label for="Harga" class="col-sm-2 col-form-label">Harga</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control <?= $validation->hasError('Harga') ? 'is-invalid' : '' ?>" id="Harga" name="Harga" value="<?= $menu['Harga'] ?>">
                    </div>
                </div>
                <div class="row mb-3">
                    <label for="Stok" class="col-sm-2 col-form-label">Stok</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control <?= $validation->hasError('Stok') ? 'is-invalid' : '' ?>" id="Stok" name="Stok" value="<?= $menu['Stok'] ?>">
                    </div>
                </div>

                <div class="d-grid gap-2 d-md-flex justify-content-md-end">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>
<?= $this->endSection() ?>